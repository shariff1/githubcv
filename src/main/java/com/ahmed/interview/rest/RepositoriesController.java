package com.ahmed.interview.rest;


import com.ahmed.interview.dto.GitAllRepositoriesResponse;
import com.ahmed.interview.dto.GitRepositoriesResponse;
import com.ahmed.interview.dto.Repository;
import com.ahmed.interview.dto.RepositoryAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RestController
public class RepositoriesController {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/api/repositories")
    public List<Repository> repositories(@RequestParam(name = "q") String query) {
        ResponseEntity<GitRepositoriesResponse> forEntity =
                restTemplate.getForEntity(String.format("https://api.github.com/search/repositories?q=%s", query), GitRepositoriesResponse.class);
        return forEntity.getBody().getItems();
    }

    @RequestMapping("/api/users/{allrepo}/repos")
    public List<RepositoryAll> getall(@PathVariable("allrepo") String allrepos) {
        ResponseEntity<GitAllRepositoriesResponse> forEntity =
                restTemplate.getForEntity(String.format("https://api.github.com/users/%s/repos", allrepos), GitAllRepositoriesResponse.class);
        return forEntity.getBody().getItems();


    }

    @RequestMapping("/api/users/{username}/repos")
    public Integer getPercentage(@PathVariable("username") String username) {
        ResponseEntity<GitAllRepositoriesResponse> forEntity =
                restTemplate.getForEntity(String.format("https://api.github.com/users/%s/repos", username), GitAllRepositoriesResponse.class);
        ArrayList<String> languageList = new ArrayList<>();
        for (String language : forEntity.getBody().getItems().getlanguage()
                ) {
            languageList.add(language);
        }
        return languageList.size() * 100 / forEntity.getBody().getItems().size();


    }

}
