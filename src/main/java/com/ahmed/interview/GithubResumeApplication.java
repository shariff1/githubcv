package com.ahmed.interview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan("com.ahmed.interview")
public class GithubResumeApplication {

	public static void main(String[] args) {
		SpringApplication.run(GithubResumeApplication.class, args);
	}
}
