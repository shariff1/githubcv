package com.ahmed.interview.dto;

import lombok.Data;

import java.util.List;

@Data
class GitResponse<T> {
    private List<T> items;
}
