package com.ahmed.interview.dto;

import lombok.Data;

@Data
public class GitRepositoriesResponse extends GitResponse<Repository> {
}
