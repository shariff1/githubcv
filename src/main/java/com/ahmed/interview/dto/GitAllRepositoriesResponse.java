package com.ahmed.interview.dto;

import lombok.Data;

@Data
public class GitAllRepositoriesResponse extends GitResponse<RepositoryAll> {
}
