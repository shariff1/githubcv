package com.ahmed.interview.dto;

import lombok.Data;

@Data
public class GitUsersResponse extends GitResponse<User> {
}
