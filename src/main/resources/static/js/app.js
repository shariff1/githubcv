var app = angular.module('app', ['ngRoute','ngResource']);
app.config(function($routeProvider){
    $routeProvider
        .when('/users',{
            templateUrl: '/views/generatedcv.html',
            controller: 'cvController'
        })
        .otherwise(
            { redirectTo: '/'}
        );
});

